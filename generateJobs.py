#!/usr/bin/python3

import sys
import os
import getopt
import yaml
from string import Template
import configparser
from configparser import DuplicateSectionError
import base64


try:
  opts, args = getopt.getopt(sys.argv[1:], "c:t:", ["config=", "template="])
except getopt.GetoptError:
  print('generateJobs.py -c <configfile> -t <templatefile>')
  sys.exit(1)

config_name = None
template_name = 'restic_backups.nomad.tpl'
for opt, arg in opts:
  if opt in ("-c", "--config"):
    config_name = arg
  elif opt in ("-t", "--template"):
    template_name = arg

if not config_name:
  print('Missing config file')
  sys.exit(1)

with open(config_name, 'r') as configfile:
  config = yaml.safe_load(configfile)

with open(template_name, 'r') as templatefile:
  template = Template(templatefile.read())

try:
  def_schedule                   = config['defaults'].get('schedule')
  def_data                       = config['defaults'].get('data')
  def_cache                      = config['defaults'].get('cache')
  def_prune_snapshots_older_than = config['defaults'].get('prune_snapshots_older_than')
  def_s3_endpoint                = config['defaults'].get('s3_endpoint')
  def_source_path                = config['defaults'].get('source_path')
  def_s3_project                 = config['defaults'].get('s3_project')
  def_s3_repository              = config['defaults'].get('s3_repository')
  def_excludes                   = config['defaults'].get('excludes')
except IndexError:
  print('Missing configuration options')
  sys.exit(2)

del config['defaults']

for job in config:
  data = {
    'JOB'                        : job,
    'SCHEDULE'                   : config[job].get('schedule', def_schedule),
    'DATA'                       : config[job].get('data', def_data),
    'CACHE'                      : config[job].get('cache', def_cache),
    'PRUNE_SNAPSHOTS_OLDER_THAN' : config[job].get('prune_snapshots_older_than', def_prune_snapshots_older_than),
    'S3_ENDPOINT'                : config[job].get('s3_endpoint', def_s3_endpoint),
    'SOURCE_PATH'                : config[job].get('source_path', def_source_path),
    'S3_PROJECT'                 : config[job].get('s3_project', def_s3_project),
    'S3_REPOSITORY'              : config[job].get('s3_repository', def_s3_repository),
    'EXCLUDES'                   : config[job].get('excludes', def_excludes),
  }

  jobfile = '{0}_{1}_backups.nomad'.format(os.getenv('PREFIX', 'dev'), job)
  with open(jobfile, 'w') as jobf:
    jobf.write(template.safe_substitute(data))
    print('Generated job: {0}'.format(jobfile))
