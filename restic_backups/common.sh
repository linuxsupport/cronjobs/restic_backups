#!/bin/bash

TODAY=$(/bin/date +%Y%m%d)
YESTERDAY=$(/bin/date +%Y%m%d -d 'yesterday')
SOURCE=/data/$SOURCE_PATH
if [ ! -d /cache/.restic ]; then
  mkdir -p /cache/.restic 2>/dev/null
fi
if [ ! -d /cache/.restic/tmpdir ]; then
  mkdir -p /cache/.restic/tmpdir 2>/dev/null
fi
if [ ! -d /cache/.restic/cachedir ]; then
  mkdir -p /cache/.restic/cachedir 2>/dev/null
fi
export TMPDIR=/cache/.restic/tmpdir
TEMPLATE=email_report.tpl
# s3.connections default is 5, let's increase it for better perf
# and set the cachedir explicitly
RESTIC="restic -o s3.connections=32 --cache-dir /cache/.restic/cachedir"
MAILMX="cernmx.cern.ch"
if [ ! -z $S3_PROJECT ]; then
  ID_VAR="${S3_PROJECT}_AWS_ACCESS_KEY_ID"
  export AWS_ACCESS_KEY_ID="${!ID_VAR}"
  SECRET_VAR="${S3_PROJECT}_AWS_SECRET_ACCESS_KEY"
  export AWS_SECRET_ACCESS_KEY="${!SECRET_VAR}"
fi
# Usually this is passed via a nomad variable
# Not when performing an interactive restore though
if [ -z $S3_ENDPOINT ]; then
  S3_ENDPOINT="s3-fr-prevessin-1.cern.ch"
fi
