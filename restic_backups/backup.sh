#!/bin/bash

set -o pipefail
source common.sh

# we want to maintain the same $SOURCE path to simplify restores, but we
# don't really want to read every snapshot on every run as this will take days
# Let's just backup $TODAY, ignoring symlink snaps (zero updates) as well
# But just in case, we'll also back up anything modified in the last 24 hours (ctime +1)
EXCLUDE_LIST=$(mktemp)
RESTIC_LOGFILE=$(mktemp)

echo "Unlocking, just in case" | tee -a $RESTIC_LOGFILE
$RESTIC unlock --remove-all 2>&1 | tee -a $RESTIC_LOGFILE

# Let the other script configure our exclude list
source ~/$EXCLUDES

echo "Performing backup..."
$RESTIC backup --tag $TODAY --exclude-file=$EXCLUDE_LIST $SOURCE 2>&1 | tee -a $RESTIC_LOGFILE
echo "Backup concluded with exit code $?"
rm -f $EXCLUDE_LIST

# Check if there are any snapshots to forget/purge
echo "Checking for old snapshots..."
SNAPS_TO_REMOVE=$($RESTIC forget --dry-run --group-by paths --keep-within $PRUNE_SNAPSHOTS_OLDER_THAN | grep "remove .* snapshots" | awk '{print $2}')
if [ ! -z $SNAPS_TO_REMOVE ]; then
  echo "Found $SNAPS_TO_REMOVE snapshots that are older than $PRUNE_SNAPSHOTS_OLDER_THAN. Purging from restic store ..." 2>&1 | tee -a $RESTIC_LOGFILE
  $RESTIC unlock 2>&1 | tee -a $RESTIC_LOGFILE
  $RESTIC forget --group-by paths --keep-within $PRUNE_SNAPSHOTS_OLDER_THAN --prune 2>&1 | tee -a $RESTIC_LOGFILE
fi

echo "Sending email of $RESTIC_LOGFILE to admins"
export TODAY="$TODAY"
JOB_PREFIX=$(echo ${NOMAD_GROUP_NAME} | sed 's/^prod_//' | sed 's/^dev_//' | sed 's/_backups$//')
export JOB_PREFIX="${JOB_PREFIX^^}"

envsubst < $TEMPLATE | sed '/^--RESTIC_LOGFILE--$/Q' > email
cat $RESTIC_LOGFILE >> email
envsubst < $TEMPLATE | sed '1,/^--RESTIC_LOGFILE--$/d' >> email
rm -f $RESTIC_LOGFILE

cat email | swaks --server $MAILMX --to $EMAIL_ADMIN --from $EMAIL_FROM --helo cern.ch --data -
rm -f email
