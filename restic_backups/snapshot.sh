#!/bin/bash

# We just want to backup the snapshots from the last two days
find $SOURCE -mindepth 1 -maxdepth 1 \( -type l -o -type d \) ! -path "*$TODAY" -ctime +1 >> $EXCLUDE_LIST
