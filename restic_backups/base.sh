#!/bin/bash

# We're only really interested in alma, centos and rhel subdirectories
find $SOURCE -mindepth 1 -maxdepth 1 ! -name 'alma' ! -name 'centos' ! -name 'rhel' >> $EXCLUDE_LIST

# But not CentOS 6 or 7
echo /data/cern/centos/6 >> $EXCLUDE_LIST
find $SOURCE/centos -mindepth 1 -maxdepth 1 -name '7*' >> $EXCLUDE_LIST

# We also don't want to backup the snapshots themselves, that will be done by the other jobs
find $SOURCE -mindepth 2 -maxdepth 2 -name '*-snapshots' >> $EXCLUDE_LIST
