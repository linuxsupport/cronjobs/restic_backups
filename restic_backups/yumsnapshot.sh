#!/bin/bash

# We just want to backup the yumsnapshot from yesterday
find $SOURCE -mindepth 1 -maxdepth 1 \( -type l -o -type d \) ! -path "*$YESTERDAY" >> $EXCLUDE_LIST

# cdn.redhat.com is ~3TB which seems excessive to read/backup every day
find $SOURCE/$YESTERDAY -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "cdn*redhat.com" >> $EXCLUDE_LIST
# debian is also excessive at ~2TB
find $SOURCE/$YESTERDAY -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "debian" >> $EXCLUDE_LIST
# A few mirrors are also excessive
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "yum.oracle.com" >> $EXCLUDE_LIST
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "packages.grafana.com" >> $EXCLUDE_LIST
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "rpm.releases.hashicorp.com" >> $EXCLUDE_LIST
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "developer.download.nvidia.com" >> $EXCLUDE_LIST
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "yum.neo4j.org" >> $EXCLUDE_LIST
find $SOURCE/$YESTERDAY/mirror -mindepth 1 -maxdepth 1 \( -type l -o -type d \) -name "artifacts.elastic.co" >> $EXCLUDE_LIST
