#!/bin/bash

source common.sh
FUSEMOUNT=fusemount

if [ ! -z $NOMAD_ADDR ]; then
  BASE_PROD="/data/cern"
else
  BASE_PROD="/mnt/data1/dist/cern"
fi

if [ ! -f /tmp/credentials ]; then
  source get_credentials.sh
fi
echo "Credentials found in /tmp/credentials, sourcing them ..."
source /tmp/credentials
echo "Working with content type: $CONTENT"

if [ ! -z $DISTRIBUTION ]; then
  RESTIC_REPO="s3:${S3_ENDPOINT}/$DISTRIBUTION-backups-production"
else
  RESTIC_REPO="s3:${S3_ENDPOINT}/$CONTENT-backups-production"
fi
if [ "$1" == "restore" ]; then
  if [ -z $2 ]; then
    echo "Error, need a date to restore [YYYYMMDD] from ..."
    exit
  fi
  if [ -z $3 ]; then
    echo "Error, need a target path to restore to ..."
    exit
  fi
  BASE_DATE=$2
  RESTORE_DIR=$3
  if [ "$CONTENT" = "aims" ] || [ "$CONTENT" = "koji" ] || [ "$CONTENT" = "yumsnapshot" ]; then
    SNAPSHOT=$($RESTIC -r $RESTIC_REPO snapshots --tag $BASE_DATE 2>/dev/null | grep $BASE_DATE | awk '{print $1}')
    if [ -z $SNAPSHOT ]; then
      echo -e "Unable to find a snapshot to restore $CONTENT content. Is the date ($BASE_DATE) correct?\nPerhaps you've configured incorrect credentials for '$CONTENT' content in /tmp/credentials ?"
      exit
    fi
    echo "***** Restoring $CONTENT (snapshot: ${BASE_DATE} / ${SNAPSHOT}) to ${RESTORE_DIR} *****"
    $RESTIC -r $RESTIC_REPO restore -t $RESTORE_DIR $SNAPSHOT
  fi
  if [ "$CONTENT" == "distribution" ]; then
    echo "Which distributions should I restore?"
    PS3="Distributions: "
    select DIST in "Everything (AlmaLinux, RHEL)" "AlmaLinux" "RHEL"
    do
      if [[ $REPLY -eq 1 ]]; then
        DISTRIBUTIONS="alma rhel"
      elif [[ $REPLY -eq 2 ]]; then
        DISTRIBUTIONS="alma"
      elif [[ $REPLY -eq 3 ]]; then
        DISTRIBUTIONS="rhel"
      fi
      break
    done
    echo "Which releases should I restore?"
    PS3="Releases: "
    select RLS in "Everything (8,8-testing,9,9-testing)" "8 Only (8,8-testing)" "9 Only (9,9-testing)" "8 production" "8 testing" "9 production" "9 testing"
    do
      if [[ $REPLY -eq 1 ]]; then
        RELEASES="8 8-testing 9 9-testing"
      elif [[ $REPLY -eq 2 ]]; then
        RELEASES="8 8-testing"
      elif [[ $REPLY -eq 3 ]]; then
        RELEASES="9 9-testing"
      elif [[ $REPLY -eq 4 ]]; then
        RELEASES="8"
      elif [[ $REPLY -eq 5 ]]; then
        RELEASES="8-testing"
      elif [[ $REPLY -eq 6 ]]; then
        RELEASES="9"
      elif [[ $REPLY -eq 7 ]]; then
        RELEASES="9-testing"
      fi
      break
    done
    echo "***** Restoring the state of the symlinks for ${DISTRIBUTIONS} / ${RELEASES} as of ${BASE_DATE} to ${RESTORE_DIR} *****"
    # base snapshot restoration
    RESTIC_REPO="s3:${S3_ENDPOINT}/base-backups-production"
    BASE_SNAPSHOT=$($RESTIC -r $RESTIC_REPO snapshots --tag $BASE_DATE 2>/dev/null | grep $BASE_DATE | awk '{print $1}')
    if [ -z $BASE_SNAPSHOT ]; then
      echo -e "Unable to find a snapshot to restore 'base' content. Is the date ($BASE_DATE) correct?\nPerhaps you've configured incorrect credentials for '$CONTENT' content in /tmp/credentials ?"
      exit
    fi
    echo "***** Restoring base symlinks to ${RESTORE_DIR} *****"
    $RESTIC restore -r $RESTIC_REPO -t $RESTORE_DIR $BASE_SNAPSHOT
    for DISTRIBUTION in $DISTRIBUTIONS
    do
      for RELEASE in $RELEASES
      do
        echo "***** Working on ${DISTRIBUTION}/${RELEASE} *****"
        RESTIC_REPO="s3:${S3_ENDPOINT}/${DISTRIBUTION}${RELEASE::1}-backups-production"
        SNAPSHOT_PATH_FULL=$(readlink ${RESTORE_DIR}/data/cern/${DISTRIBUTION}/${RELEASE})
        echo "***** Restoring ${DISTRIBUTION}/${RELEASE} (snapshot: ${SNAPSHOT_PATH_FULL}) to ${RESTORE_DIR} *****"
        SNAPSHOT_PATH_SHORT=$(echo $SNAPSHOT_PATH_FULL | cut -d \/ -f2)
        DISTRIBUTION_SNAPSHOT=$($RESTIC -r $RESTIC_REPO snapshots --tag $SNAPSHOT_PATH_SHORT | grep $SNAPSHOT_PATH_SHORT | awk '{print $1}')
        $RESTIC -r $RESTIC_REPO restore -t $RESTORE_DIR --include /data/cern/${DISTRIBUTION}/${SNAPSHOT_PATH_FULL} $DISTRIBUTION_SNAPSHOT
      done
    done
  fi
elif [ "$1" == "snapshots" ]; then
  $RESTIC -r $RESTIC_REPO snapshots
elif [ "$1" == "unlock" ]; then
  $RESTIC -r $RESTIC_REPO unlock
elif [ "$1" == "mount" ]; then
  # TODO: check fusemount is not already mounted
  if [ ! -d "$FUSEMOUNT" ]; then
    mkdir $FUSEMOUNT
  fi
  echo "mounting \"$FUSEMOUNT\" (process backgrounded)"
  echo "use: '$0 unmount' when finished"
  $RESTIC -r $RESTIC_REPO mount $FUSEMOUNT &> /dev/null &
elif [ "$1" == "unmount" ]; then
  pkill -f $RESTIC &> /dev/null
  fusermount -u $FUSEMOUNT &> /dev/null
elif [ "$1" == "quota" ]; then
  JSON=$(./s3_usage_check  -endpoint https://s3-fr-prevessin-1.cern.ch -json)
  TOTAL_QUOTA=$(echo $JSON |  jq '.Summary[0]' | awk '{printf "%d", $1 / 1073741824}')
  CONSUMED_QUOTA=$(echo $JSON | jq '.Summary[5]' | awk '{printf "%d", $1 / 1073741824}')
  echo "$CONTENT: $CONSUMED_QUOTA GB / $TOTAL_QUOTA GB consumed"

  BUCKETS=$(echo $JSON | jq -c '.CapacityUsed[0].Buckets[] | {Bucket, Bytes}')
  echo "$BUCKETS" | while IFS= read -r BUCKET; do
    NAME=$(echo $BUCKET | jq -r '.Bucket')
    BYTES=$(echo $BUCKET | jq -r '.Bytes')
    GIGABYTES=$(awk -v BYTES="$BYTES" 'BEGIN {printf "%d", BYTES / 1073741824}')
    echo "$NAME: $GIGABYTES GB"
  done
else
  echo "usage: $0 <restore YYYYMMDD path|unlock|mount|unmount|snapshots|quota>"
fi
